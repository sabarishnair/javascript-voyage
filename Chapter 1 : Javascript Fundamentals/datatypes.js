// Primitive Data Types
let age = 25;
let name = "John";
let isStudent = true;
let emptyValue = null;
let undefinedValue;

// Complex Data Types
let person = {
  firstName: "Alice",
  lastName: "Johnson"
};

function greet(name) {
  return `Hello, ${name}!`;
}
